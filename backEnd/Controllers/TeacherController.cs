﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SchoolManager.Model;
using SchoolManager.Repository;

namespace SchoolManager.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeacherController : Controller
    {
        private ITeacherRepo _teacherRepo;
        public TeacherController(ITeacherRepo teacherRepo)
        {
            _teacherRepo = teacherRepo;
        }
        [Route("getAll")]
        [HttpGet]
        public IActionResult GetAllTeachers(int page)
        {
           IEnumerable<Teacher> teachers = _teacherRepo.GetAllTeachers(page);
            if (teachers != null)
            {
                return Ok(teachers);
            }
            return BadRequest("Check TeacherController GetAll method!");
        }
        [Route("getById")]
        [HttpGet]
        public IActionResult GetTeacher(int id)
        {
            Teacher teacher = _teacherRepo.GetTeacher(id);
            if (teacher != null)
            {
                return Ok(teacher);
            }
            return BadRequest("Check TeacherController GetTeacher method!");
        }
        [Route("getTeacherDetails")]
        [HttpGet]
        public IActionResult GetTeacherDetails(int id)
        {
            IEnumerable <TeacherDetails> teacherDetails = _teacherRepo.GetTeacherDetails(id);
            if (teacherDetails != null)
            {
                return Ok(teacherDetails);
            }
            return BadRequest("Check TeacherController GetTeacherDetails method!");
        }
    }
}