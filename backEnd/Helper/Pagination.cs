﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManager.Helper
{
    public static class Pagination
    {
        public static int PageNumber { get; set; } = 1;
    }
}
