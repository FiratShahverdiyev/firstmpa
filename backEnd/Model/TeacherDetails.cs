﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManager.Model
{
    public class TeacherDetails
    {
        public int TeacherId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Lesson { get; set; }
        public int DayOfWeek { get; set; }
        public DateTime Start_At { get; set; }
        public DateTime End_At { get; set; }
        public int RoomNumber { get; set; }
        public string GroupName{ get; set; }
    }
}
