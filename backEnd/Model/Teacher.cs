﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManager.Model
{
    public class Teacher
    {
       public int TeacherId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
