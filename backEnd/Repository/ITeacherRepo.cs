﻿using SchoolManager.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManager.Repository
{
   public interface ITeacherRepo
    {
        public IEnumerable<Teacher> GetAllTeachers(int page);
        public Teacher GetTeacher(int id);
        public IEnumerable<TeacherDetails> GetTeacherDetails(int id);
    }
}
