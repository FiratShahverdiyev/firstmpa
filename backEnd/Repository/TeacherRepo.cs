﻿using SchoolManager.Helper;
using SchoolManager.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManager.Repository
{
    public class TeacherRepo : ITeacherRepo
    {
        string connection = "Data source=DESKTOP-PMH3J5I\\DB1; Initial Catalog=ManagmentFinal ; Integrated Security=true";
        List<Teacher> teachers = new List<Teacher>();
        List<TeacherDetails> teacherDetails = new List<TeacherDetails>();
        public IEnumerable<Teacher> GetAllTeachers(int page)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connection))
            {
                Pagination.PageNumber = page;
                sqlConnection.Open();
                teachers.Clear();
                string query = "Select * from Teachers";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.CommandType = CommandType.Text;
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    Teacher teacher = new Teacher();
                    teacher.TeacherId = Convert.ToInt32(dataReader["Id"]);
                    teacher.FirstName = dataReader["Firstname"].ToString();
                    teacher.LastName = dataReader["Lastname"].ToString();
                    teachers.Add(teacher);
                }
                dataReader.Close();
                sqlConnection.Close();
                return teachers.Skip((Pagination.PageNumber-1)*5).Take(5);
            }
        }

        public Teacher GetTeacher(int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connection))
            {
                sqlConnection.Open();
                teachers.Clear();
                string query = "Select * from Teachers where Id=@Id";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.CommandType = CommandType.Text;
                command.Parameters.AddWithValue("@Id", id);
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    Teacher teacher = new Teacher();
                    teacher.TeacherId = Convert.ToInt32(dataReader["Id"]);
                    teacher.FirstName = dataReader["Firstname"].ToString();
                    teacher.LastName = dataReader["Lastname"].ToString();
                    dataReader.Close();
                    sqlConnection.Close();
                    return teacher;
                }
                return null;
            }
        }

        public IEnumerable<TeacherDetails> GetTeacherDetails(int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connection))
            {
                sqlConnection.Open();
                teachers.Clear();
                teacherDetails.Clear();
                string query = "select T.Id, T.FirstName , T.LastName , L.Lesson , TD.Start_at , TD.End_at , ED.Day , G.GroupName , R.RoomNumber from Teachers T "+
                " inner join Teachers_Lessons TL on TL.TeacherId = T.Id "+
                 "inner join Lessons L on L.Id = TL.LessonId "+
                 "inner join Rooms R on R.Id = TL.RoomId "+
                 "inner join ExactDate ED on ED.Id = TL.ExactDateId "+
                 "inner join TimesDates TD on ED.TimeId = TD.Id "+
                 "inner join Groups G on G.Id = TL.GroupId "+
                       " where T.Id = @Id ";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.CommandType = CommandType.Text;
                command.Parameters.AddWithValue("@Id", id);
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    TeacherDetails teacherDetail = new TeacherDetails();
                    teacherDetail.TeacherId = Convert.ToInt32(dataReader["Id"]);
                    teacherDetail.FirstName = dataReader["Firstname"].ToString();
                    teacherDetail.LastName = dataReader["Lastname"].ToString();
                    teacherDetail.Lesson = dataReader["Lesson"].ToString();
                    teacherDetail.Start_At = DateTime.Parse(dataReader["Start_at"].ToString());
                    teacherDetail.End_At = DateTime.Parse(dataReader["End_at"].ToString());
                    teacherDetail.DayOfWeek = Convert.ToInt32(dataReader["Day"]);
                    teacherDetail.RoomNumber = Convert.ToInt32(dataReader["RoomNumber"]);
                    teacherDetail.GroupName = dataReader["GroupName"].ToString();
                    teacherDetails.Add(teacherDetail);
                }
                dataReader.Close();
                sqlConnection.Close();
                return teacherDetails;

            }
        }
    }
}
