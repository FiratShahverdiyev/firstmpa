import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter);
import HomePage from './components/HomePage.vue'
import TeacherList from './components/TeacherList.vue'
import TeacherInfo from './components/TeacherInfo.vue'
Vue.config.productionTip = false

const router=new VueRouter({
  routes : [
    {path : '/' ,component: HomePage},
    {path : '/Teacher', component: TeacherList},
    {path : '/Teacher/:teacherId' , component : TeacherInfo}
     ],
     mode : 'history',

    })
new Vue({
  render: h => h(App),
  router
}).$mount('#app')
